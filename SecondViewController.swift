//
//  SecondViewController.swift
//  To do list
//
//  Created by Click Labs on 1/20/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    //text field variable...
    @IBOutlet weak var toDoItem: UITextField!
    
    var markCompleteOnDragRelease = false;
    
    @IBOutlet weak var errorLabel: UILabel!
    
    //this code is execute when add button is clicked...
    @IBAction func addTodoList(sender: AnyObject) {
        
        if toDoItem.text != ""{
            //append the new item in table...
            toDoItems.append(toDoItem.text)
            
            //these three lines are used to store the items in table permanently...
            //let a contant...
            let fixedtoDoItem = toDoItems
            NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItem, forKey: "toDoItems")
            NSUserDefaults.standardUserDefaults().synchronize()
        
            //when add is pressed then text field gets nil...
            toDoItem.text = ""
            errorLabel.hidden = true
        }
        else
        {
            errorLabel.hidden = false
            errorLabel.text = "Please enter item..."
        }
        
        //when add is pressed then keyboard gets hide...
        self.view.endEditing(true)
    }
    
    //when return button is pressed in keyboard then keyboard gets hide...
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    
    }
    
    //when touches outside of textfield keyboard gets hide...
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        self.view.endEditing(true)
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

