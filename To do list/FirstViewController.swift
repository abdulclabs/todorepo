//
//  FirstViewController.swift
//  To do list
//
//  Created by Click Labs on 1/20/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

 //let an array to store items...
 var toDoItems:[String] = []

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
        //let a outlet variable to refer the tableView...
        @IBOutlet var taskTable:UITableView!
    
        //function which counts  number of rows in tableView and return...
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            
                        return toDoItems.count
    
        }
    
        //function which convert all rows of table into cells and retuns all cells...
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    
                        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                        cell.textLabel?.text = toDoItems[indexPath.row]
            
                        return cell
    
        }

        //when this function run table reload again...
        override func viewWillAppear(animated: Bool) {
            
                //let an empty array which store all items as object...
                toDoItems = []
            
                //convert all itmes into object and store in a variable...
                if var storedtoDoItems: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("toDoItems"){
            
                        for var i = 0; i < storedtoDoItems.count; ++i {
                            toDoItems.append(storedtoDoItems[i] as NSString)
                        }
                }
            
                //reload the tableView...
                taskTable.reloadData()
        }
    
        //function which is used to delete the cell on sliding left...
        func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
                if editingStyle == UITableViewCellEditingStyle.Delete {
            
                        toDoItems.removeAtIndex(indexPath.row)
                        taskTable.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                    
                        //this code is used to delete the cell permanently...
                        let fixedtoDoItem = toDoItems
                        NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItem, forKey: "toDoItems")
                        NSUserDefaults.standardUserDefaults().synchronize()

                }
        }
    
            
        override func viewDidLoad() {
                        super.viewDidLoad()
                        // Do any additional setup after loading the view, typically from a nib.
        }
    
        override func didReceiveMemoryWarning() {
                        super.didReceiveMemoryWarning()
                        // Dispose of any resources that can be recreated.
        }


}

 